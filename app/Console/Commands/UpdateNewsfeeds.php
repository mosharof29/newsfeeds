<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Settings;
use App\Helpers\NewsfeedsHelper;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationMail;

class UpdateNewsfeeds extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsfeeds:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will fetch data from newsfeed api';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $all_settings = Settings::with('user')->get();
        $newsfeedHelper = new NewsfeedsHelper;

        foreach($all_settings as $settings)
        {
            $newsfeedHelper->fetchArticlesAndSave($settings);
            Mail::to($settings->user->email)->send(new NotificationMail($settings->user));
        }
        
        \Log::info("Newsfeed has ben updated successfully!");
    }
}
