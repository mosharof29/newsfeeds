<?php

function countries()
{
	return [
		"ar" =>	"Argentina",
		"au" =>	"Australia",
		"at" =>	"Austria",
		"be" =>	"Belgium",
		"br" =>	"Brazil",
		"bg" =>	"Bulgaria",
		"ca" =>	"Canada",
		"cn" =>	"China",
		"co" =>	"Colombia",
		"cu" =>	"Cuba",
		"cz" =>	"Czech Republic",
		"eg" =>	"Egypt",
		"fr" =>	"France",
		"de" =>	"Germany",
		"gr" =>	"Greece",
		"hk" =>	"Hong Kong",
		"hu" =>	"Hungary",
		"in" =>	"India",
		"id" =>	"Indonesia",
		"ie" =>	"Ireland",
		"il" =>	"Israel",
		"it" =>	"Italy",
		"jp" =>	"Japan",
		"lv" =>	"Latvia",
		"lt" =>	"Lithuania",
		"my" =>	"Malaysia",
		"mx" =>	"Mexico",
		"ma" =>	"Morocco",
		"nl" =>	"Netherlands",
		"nz" =>	"New Zealand",
		"ng" =>	"Nigeria",
		"no" =>	"Norway",
		"ph" =>	"Philippines",
		"pl" =>	"Poland",
		"pt" =>	"Portugal",
		"ro" =>	"Romania",
		"ru" =>	"Russia",
		"sa" =>	"Saudi Arabia",
		"rs" =>	"Serbia",
		"sg" =>	"Singapore",
		"sk" =>	"Slovakia",
		"si" =>	"Slovenia",
		"za" =>	"South Africa",
		"kr" =>	"South Korea",
		"se" =>	"Sweden",
		"ch" =>	"Switzerland",
		"tw" =>	"Taiwan",
		"th" =>	"Thailand",
		"tr" =>	"Turkey",
		"ae" =>	"UAE",
		"ua" =>	"Ukraine",
		"gb" =>	"United Kingdom",
		"us" =>	"United States",
		"ve" =>	"Venuzuela"
	];
}

function sources()
{
	return [
	    [
	        "id" => "abc-news",
	        "name" => "ABC News",
	        "country" => "us"
	    ],
	    [
	        "id" => "abc-news-au",
	        "name" => "ABC News (AU)",
	        "country" => "au"
	    ],
	    [
	        "id" => "aftenposten",
	        "name" => "Aftenposten",
	        "country" => "no"
	    ],
	    [
	        "id" => "al-jazeera-english",
	        "name" => "Al Jazeera English",
	        "country" => "us"
	    ],
	    [
	        "id" => "ansa",
	        "name" => "ANSA.it",
	        "country" => "it"
	    ],
	    [
	        "id" => "argaam",
	        "name" => "Argaam",
	        "country" => "sa"
	    ],
	    [
	        "id" => "ars-technica",
	        "name" => "Ars Technica",
	        "country" => "us"
	    ],
	    [
	        "id" => "ary-news",
	        "name" => "Ary News",
	        "country" => "pk"
	    ],
	    [
	        "id" => "associated-press",
	        "name" => "Associated Press",
	        "country" => "us"
	    ],
	    [
	        "id" => "australian-financial-review",
	        "name" => "Australian Financial Review",
	        "country" => "au"
	    ],
	    [
	        "id" => "axios",
	        "name" => "Axios",
	        "country" => "us"
	    ],
	    [
	        "id" => "bbc-news",
	        "name" => "BBC News",
	        "country" => "gb"
	    ],
	    [
	        "id" => "bbc-sport",
	        "name" => "BBC Sport",
	        "country" => "gb"
	    ],
	    [
	        "id" => "bild",
	        "name" => "Bild",
	        "country" => "de"
	    ],
	    [
	        "id" => "blasting-news-br",
	        "name" => "Blasting News (BR)",
	        "country" => "br"
	    ],
	    [
	        "id" => "bleacher-report",
	        "name" => "Bleacher Report",
	        "country" => "us"
	    ],
	    [
	        "id" => "bloomberg",
	        "name" => "Bloomberg",
	        "country" => "us"
	    ],
	    [
	        "id" => "breitbart-news",
	        "name" => "Breitbart News",
	        "country" => "us"
	    ],
	    [
	        "id" => "business-insider",
	        "name" => "Business Insider",
	        "country" => "us"
	    ],
	    [
	        "id" => "business-insider-uk",
	        "name" => "Business Insider (UK)",
	        "country" => "gb"
	    ],
	    [
	        "id" => "buzzfeed",
	        "name" => "Buzzfeed",
	        "country" => "us"
	    ],
	    [
	        "id" => "cbc-news",
	        "name" => "CBC News",
	        "country" => "ca"
	    ],
	    [
	        "id" => "cbs-news",
	        "name" => "CBS News",
	        "country" => "us"
	    ],
	    [
	        "id" => "cnn",
	        "name" => "CNN",
	        "country" => "us"
	    ],
	    [
	        "id" => "cnn-es",
	        "name" => "CNN Spanish",
	        "country" => "us"
	    ],
	    [
	        "id" => "crypto-coins-news",
	        "name" => "Crypto Coins News",
	        "country" => "us"
	    ],
	    [
	        "id" => "der-tagesspiegel",
	        "name" => "Der Tagesspiegel",
	        "country" => "de"
	    ],
	    [
	        "id" => "die-zeit",
	        "name" => "Die Zeit",
	        "country" => "de"
	    ],
	    [
	        "id" => "el-mundo",
	        "name" => "El Mundo",
	        "country" => "es"
	    ],
	    [
	        "id" => "engadget",
	        "name" => "Engadget",
	        "country" => "us"
	    ],
	    [
	        "id" => "entertainment-weekly",
	        "name" => "Entertainment Weekly",
	        "country" => "us"
	    ],
	    [
	        "id" => "espn",
	        "name" => "ESPN",
	        "country" => "us"
	    ],
	    [
	        "id" => "espn-cric-info",
	        "name" => "ESPN Cric Info",
	        "country" => "us"
	    ],
	    [
	        "id" => "financial-post",
	        "name" => "Financial Post",
	        "country" => "ca"
	    ],
	    [
	        "id" => "focus",
	        "name" => "Focus",
	        "country" => "de"
	    ],
	    [
	        "id" => "football-italia",
	        "name" => "Football Italia",
	        "country" => "it"
	    ],
	    [
	        "id" => "fortune",
	        "name" => "Fortune",
	        "country" => "us"
	    ],
	    [
	        "id" => "four-four-two",
	        "name" => "FourFourTwo",
	        "country" => "gb"
	    ],
	    [
	        "id" => "fox-news",
	        "name" => "Fox News",
	        "country" => "us"
	    ],
	    [
	        "id" => "fox-sports",
	        "name" => "Fox Sports",
	        "country" => "us"
	    ],
	    [
	        "id" => "globo",
	        "name" => "Globo",
	        "country" => "br"
	    ],
	    [
	        "id" => "google-news",
	        "name" => "Google News",
	        "country" => "us"
	    ],
	    [
	        "id" => "google-news-ar",
	        "name" => "Google News (Argentina)",
	        "country" => "ar"
	    ],
	    [
	        "id" => "google-news-au",
	        "name" => "Google News (Australia)",
	        "country" => "au"
	    ],
	    [
	        "id" => "google-news-br",
	        "name" => "Google News (Brasil)",
	        "country" => "br"
	    ],
	    [
	        "id" => "google-news-ca",
	        "name" => "Google News (Canada)",
	        "country" => "ca"
	    ],
	    [
	        "id" => "google-news-fr",
	        "name" => "Google News (France)",
	        "country" => "fr"
	    ],
	    [
	        "id" => "google-news-in",
	        "name" => "Google News (India)",
	        "country" => "in"
	    ],
	    [
	        "id" => "google-news-is",
	        "name" => "Google News (Israel)",
	        "country" => "is"
	    ],
	    [
	        "id" => "google-news-it",
	        "name" => "Google News (Italy)",
	        "country" => "it"
	    ],
	    [
	        "id" => "google-news-ru",
	        "name" => "Google News (Russia)",
	        "country" => "ru"
	    ],
	    [
	        "id" => "google-news-sa",
	        "name" => "Google News (Saudi Arabia)",
	        "country" => "sa"
	    ],
	    [
	        "id" => "google-news-uk",
	        "name" => "Google News (UK)",
	        "country" => "gb"
	    ],
	    [
	        "id" => "goteborgs-posten",
	        "name" => "Göteborgs-Posten",
	        "country" => "se"
	    ],
	    [
	        "id" => "gruenderszene",
	        "name" => "Gruenderszene",
	        "country" => "de"
	    ],
	    [
	        "id" => "hacker-news",
	        "name" => "Hacker News",
	        "country" => "us"
	    ],
	    [
	        "id" => "handelsblatt",
	        "name" => "Handelsblatt",
	        "country" => "de"
	    ],
	    [
	        "id" => "ign",
	        "name" => "IGN",
	        "country" => "us"
	    ],
	    [
	        "id" => "il-sole-24-ore",
	        "name" => "Il Sole 24 Ore",
	        "country" => "it"
	    ],
	    [
	        "id" => "independent",
	        "name" => "Independent",
	        "country" => "gb"
	    ],
	    [
	        "id" => "infobae",
	        "name" => "Infobae",
	        "country" => "ar"
	    ],
	    [
	        "id" => "info-money",
	        "name" => "InfoMoney",
	        "country" => "br"
	    ],
	    [
	        "id" => "la-gaceta",
	        "name" => "La Gaceta",
	        "country" => "ar"
	    ],
	    [
	        "id" => "la-nacion",
	        "name" => "La Nacion",
	        "country" => "ar"
	    ],
	    [
	        "id" => "la-repubblica",
	        "name" => "La Repubblica",
	        "country" => "it"
	    ],
	    [
	        "id" => "le-monde",
	        "name" => "Le Monde",
	        "country" => "fr"
	    ],
	    [
	        "id" => "lenta",
	        "name" => "Lenta",
	        "country" => "ru"
	    ],
	    [
	        "id" => "lequipe",
	        "name" => "L'equipe",
	        "country" => "fr"
	    ],
	    [
	        "id" => "les-echos",
	        "name" => "Les Echos",
	        "country" => "fr"
	    ],
	    [
	        "id" => "liberation",
	        "name" => "Libération",
	        "country" => "fr"
	    ],
	    [
	        "id" => "marca",
	        "name" => "Marca",
	        "country" => "es"
	    ],
	    [
	        "id" => "mashable",
	        "name" => "Mashable",
	        "country" => "us"
	    ],
	    [
	        "id" => "medical-news-today",
	        "name" => "Medical News Today",
	        "country" => "us"
	    ],
	    [
	        "id" => "msnbc",
	        "name" => "MSNBC",
	        "country" => "us"
	    ],
	    [
	        "id" => "mtv-news",
	        "name" => "MTV News",
	        "country" => "us"
	    ],
	    [
	        "id" => "mtv-news-uk",
	        "name" => "MTV News (UK)",
	        "country" => "gb"
	    ],
	    [
	        "id" => "national-geographic",
	        "name" => "National Geographic",
	        "country" => "us"
	    ],
	    [
	        "id" => "national-review",
	        "name" => "National Review",
	        "country" => "us"
	    ],
	    [
	        "id" => "nbc-news",
	        "name" => "NBC News",
	        "country" => "us"
	    ],
	    [
	        "id" => "news24",
	        "name" => "News24",
	        "country" => "za"
	    ],
	    [
	        "id" => "new-scientist",
	        "name" => "New Scientist",
	        "country" => "us"
	    ],
	    [
	        "id" => "news-com-au",
	        "name" => "News.com.au",
	        "country" => "au"
	    ],
	    [
	        "id" => "newsweek",
	        "name" => "Newsweek",
	        "country" => "us"
	    ],
	    [
	        "id" => "new-york-magazine",
	        "name" => "New York Magazine",
	        "country" => "us"
	    ],
	    [
	        "id" => "next-big-future",
	        "name" => "Next Big Future",
	        "country" => "us"
	    ],
	    [
	        "id" => "nfl-news",
	        "name" => "NFL News",
	        "country" => "us"
	    ],
	    [
	        "id" => "nhl-news",
	        "name" => "NHL News",
	        "country" => "us"
	    ],
	    [
	        "id" => "nrk",
	        "name" => "NRK",
	        "country" => "no"
	    ],
	    [
	        "id" => "politico",
	        "name" => "Politico",
	        "country" => "us"
	    ],
	    [
	        "id" => "polygon",
	        "name" => "Polygon",
	        "country" => "us"
	    ],
	    [
	        "id" => "rbc",
	        "name" => "RBC",
	        "country" => "ru"
	    ],
	    [
	        "id" => "recode",
	        "name" => "Recode",
	        "country" => "us"
	    ],
	    [
	        "id" => "reddit-r-all",
	        "name" => "Reddit /r/all",
	        "country" => "us"
	    ],
	    [
	        "id" => "reuters",
	        "name" => "Reuters",
	        "country" => "us"
	    ],
	    [
	        "id" => "rt",
	        "name" => "RT",
	        "country" => "ru"
	    ],
	    [
	        "id" => "rte",
	        "name" => "RTE",
	        "country" => "ie"
	    ],
	    [
	        "id" => "rtl-nieuws",
	        "name" => "RTL Nieuws",
	        "country" => "nl"
	    ],
	    [
	        "id" => "sabq",
	        "name" => "SABQ",
	        "country" => "sa"
	    ],
	    [
	        "id" => "spiegel-online",
	        "name" => "Spiegel Online",
	        "country" => "de"
	    ],
	    [
	        "id" => "svenska-dagbladet",
	        "name" => "Svenska Dagbladet",
	        "country" => "se"
	    ],
	    [
	        "id" => "t3n",
	        "name" => "T3n",
	        "country" => "de"
	    ],
	    [
	        "id" => "talksport",
	        "name" => "TalkSport",
	        "country" => "gb"
	    ],
	    [
	        "id" => "techcrunch",
	        "name" => "TechCrunch",
	        "country" => "us"
	    ],
	    [
	        "id" => "techcrunch-cn",
	        "name" => "TechCrunch (CN)",
	        "country" => "zh"
	    ],
	    [
	        "id" => "techradar",
	        "name" => "TechRadar",
	        "country" => "us"
	    ],
	    [
	        "id" => "the-american-conservative",
	        "name" => "The American Conservative",
	        "country" => "us"
	    ],
	    [
	        "id" => "the-globe-and-mail",
	        "name" => "The Globe And Mail",
	        "country" => "ca"
	    ],
	    [
	        "id" => "the-hill",
	        "name" => "The Hill",
	        "country" => "us"
	    ],
	    [
	        "id" => "the-hindu",
	        "name" => "The Hindu",
	        "country" => "in"
	    ],
	    [
	        "id" => "the-huffington-post",
	        "name" => "The Huffington Post",
	        "country" => "us"
	    ],
	    [
	        "id" => "the-irish-times",
	        "name" => "The Irish Times",
	        "country" => "ie"
	    ],
	    [
	        "id" => "the-jerusalem-post",
	        "name" => "The Jerusalem Post",
	        "country" => "is"
	    ],
	    [
	        "id" => "the-lad-bible",
	        "name" => "The Lad Bible",
	        "country" => "gb"
	    ],
	    [
	        "id" => "the-next-web",
	        "name" => "The Next Web",
	        "country" => "us"
	    ],
	    [
	        "id" => "the-sport-bible",
	        "name" => "The Sport Bible",
	        "country" => "gb"
	    ],
	    [
	        "id" => "the-times-of-india",
	        "name" => "The Times of India",
	        "country" => "in"
	    ],
	    [
	        "id" => "the-verge",
	        "name" => "The Verge",
	        "country" => "us"
	    ],
	    [
	        "id" => "the-wall-street-journal",
	        "name" => "The Wall Street Journal",
	        "country" => "us"
	    ],
	    [
	        "id" => "the-washington-post",
	        "name" => "The Washington Post",
	        "country" => "us"
	    ],
	    [
	        "id" => "the-washington-times",
	        "name" => "The Washington Times",
	        "country" => "us"
	    ],
	    [
	        "id" => "time",
	        "name" => "Time",
	        "country" => "us"
	    ],
	    [
	        "id" => "usa-today",
	        "name" => "USA Today",
	        "country" => "us"
	    ],
	    [
	        "id" => "vice-news",
	        "name" => "Vice News",
	        "country" => "us"
	    ],
	    [
	        "id" => "wired",
	        "name" => "Wired",
	        "country" => "us"
	    ],
	    [
	        "id" => "wired-de",
	        "name" => "Wired.de",
	        "country" => "de"
	    ],
	    [
	        "id" => "wirtschafts-woche",
	        "name" => "Wirtschafts Woche",
	        "country" => "de"
	    ],
	    [
	        "id" => "xinhua-net",
	        "name" => "Xinhua Net",
	        "country" => "zh"
	    ],
	    [
	        "id" => "ynet",
	        "name" => "Ynet",
	        "country" => "is"
	    ]
	];
}