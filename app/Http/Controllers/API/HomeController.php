<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Helpers\NewsfeedsHelper;

class HomeController extends Controller
{
    public function home()
    {
        $newsfeedHelper = new NewsfeedsHelper;
        $articles = $newsfeedHelper->fetchArticles();

        return response()->json($articles);
    }
}
