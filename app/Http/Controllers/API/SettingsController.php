<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Settings;
use App\Models\Source;
use Validator;

use App\Helpers\NewsfeedsHelper;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sources = Source::get();

        if(count($sources) == 0)
        {
            $newsfeedHelper = new NewsfeedsHelper;
            $newsfeedHelper->fetchSourcesAndSave();
            $sources = Source::get();
        }

        $settings = Settings::where('user_id',Auth::user()->id)->first();

        return response()->json($settings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'countries' => 'required',
            'sources' => 'required'
        ]);

        //return $request;

        if($validator->fails()){
            return response()->json($validator->errors());       
        }

        $settings = Settings::where('user_id',Auth::user()->id)->first();

        if(!$settings)
        {
            $settings = new Settings;
            $settings->user_id = Auth::user()->id;
            $settings->save();
        }

        $settings->countries = implode(',', $request->countries);
        $settings->sources = implode(',', $request->sources);
        $settings->keywords = str_replace(' ', '', $request->keywords);
        $settings->update();

        // update articles
        $newsfeedHelper = new NewsfeedsHelper;
        $newsfeedHelper->fetchArticlesAndSave($settings);

        return response()->json($settings);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
