<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\NewsfeedsHelper;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function home()
    {
        $newsfeedHelper = new NewsfeedsHelper;
        $articles = $newsfeedHelper->fetchArticles();
        return view('home', compact('articles'));
    }
}
