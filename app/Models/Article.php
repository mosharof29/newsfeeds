<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Source;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['source_id','title','url','urlToImage','publishAt'];

    public function source()
    {
        return $this->belongsTo(Source::class,'source_id','source_id');
    }
}
