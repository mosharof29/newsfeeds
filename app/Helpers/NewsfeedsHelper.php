<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\GuzzleHttp\Client;

use App\Models\Source;
use App\Models\Article;

class NewsfeedsHelper
{
    public function fetchArticles()
    {
        // check user settings
        $settings = Auth::user()->settings;

        if(!$settings)
        {
            return $this->getLiveArticles();
        }else{
            return $this->getStoredArticles($settings->user_id);
        }
    }

    public function getLiveArticles()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://newsapi.org/v2/everything?q=keyword&apiKey=d282af872e024f0b8c52e87342059e90');
        $newsfeeds = json_decode($request->getBody(), true);
        return $newsfeeds['articles'];
    }

    public function getStoredArticles($userId)
    {
        return Article::with('source')->where('user_id',$userId)->paginate(10);
    }

    public function fetchArticlesAndSave($settings)
    {
        $client = new \GuzzleHttp\Client();
        $baseUrl = 'https://newsapi.org/v2/top-headlines?';
        $apiKey = 'd282af872e024f0b8c52e87342059e90';
        
        if(!empty($settings->keywords))
        {
            $baseUrl .= 'q='.$settings->keywords.'&';
        }

        $baseUrl .= 'sources='.$settings->sources.'&sortBy=publishedAt&apiKey='.$apiKey;

        $request = $client->get($baseUrl);
        $newsfeeds = json_decode($request->getBody(), true);

        Article::where('user_id',$settings->user_id)->delete();

        foreach ($newsfeeds['articles'] as $a => $get_article) {
            $article = new Article;
            $article->user_id = $settings->user_id;
            $article->source_id = $get_article['source']['id'];
            $article->title = $get_article['title'];
            $article->url = $get_article['url'];
            $article->urlToImage = $get_article['urlToImage'];
            $article->publishedAt = date('Y-m-d h:i:s',strtotime($get_article['publishedAt']));
            $article->save();
        }

        return true;
    }

    public function fetchSourcesAndSave()
    {
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://newsapi.org/v2/top-headlines/sources?apiKey=d282af872e024f0b8c52e87342059e90');
        $sources = json_decode($request->getBody(), true);

        foreach ($sources['sources'] as $s => $source) {
            $setSource = Source::where('source_id',$source['id'])->first();

            if(!$setSource)
            {
                $setSource = new Source;
                $setSource->source_id = $source['id'];
                $setSource->name = $source['name'];
                $setSource->domain = $source['url'];
                $setSource->country = $source['country'];
                $setSource->save();
            }
        }

        return true;
    }
}