<!DOCTYPE html>
<html>
    <head>
        <title>{{ config('app.name') }} - Notification Email</title>
    </head>
    <body>
        <p>Hello, {{ $user->name }}</p> <br>
        <p>Your newsfeeds are updated. Please check your newsfeed page to get the updated articles.</p> <br><br>
        <strong>Thank you Sir. :)</strong>
    </body>
</html>