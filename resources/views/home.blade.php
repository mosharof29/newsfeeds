@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-white">
                        <h4 class="mt-2">
                            Top Headlines
                            @if(Auth::user()->settings)
                                <small class="text-primary">(Displaying articles based on user settings)</small>
                            @else
                                <small class="text-primary">(Displaying live articles as user does not applied settings yet)</small>
                            @endif
                            <small class="pull-right">Scope: <span class="text-primary">Private</span>, Reader: <span class="text-primary">{{ Auth::user()->name }}</span></small>
                        </h4>
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="row">
                            @if(count($articles) > 0)
                                @foreach($articles as $n => $article)
                                    <div class="col-lg-4 col-md-6 mb-3">
                                        <div class="card">
                                            <img class="card-img-top" src="{{ !empty($article['urlToImage']) ? $article['urlToImage'] : asset('img/image-not-found.png') }}" alt="{{ $article['title'] }}">
                                            <div class="card-body">
                                                <h4 class="card-title">{{ $article['title'] }}</h4>
                                                <div class="text-primary">
                                                    <small>Source: {{ $article['source']['name'] ? $article['source']['name'] : 'Source not found' }}</small> <br>
                                                    <small>Country: </small>
                                                    @if($article['source'] && !empty($article['source']['country']))
                                                        <small class="text-success">{{ countries()[$article['source']['country']] }}</small>
                                                    @else
                                                        <small class="text-danger">Not Available</small>
                                                    @endif
                                                    <small class="pull-right">{{ $article['publishedAt'] }}</small>
                                                </div>
                                                <a href="{{ $article['url'] }}" target="_blank" class="card-link pull-right">read more ...</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                @if(Auth::check() && !empty(Auth::user()->settings))
                                
                                {{ $articles->links('pagination::bootstrap-4') }}

                                @endif
                            @endif
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
