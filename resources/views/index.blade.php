@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg-white text-center">
                        <h4 class="mt-5">
                            This is a sample newsfeed portal.
                        </h4>
                        <h4 class="mt-5 mb-5">
                            Please, register / login to see the newsfeeds.
                        </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
