@extends('layouts.app')

@push('styles')
    <link href="{{ asset('plugins/select2/select2.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @if (session('status'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        @endif
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header bg-white">
                    <h4 class="mt-2">
                        Settings
                    </h4>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <h5>Selected Countries <small class="text-secondary">(2-letter ISO 3166-1 code)</small></h5>
                            @if(!empty($settings) && !empty($settings->countries))
                                @php $countries = explode(',', $settings->countries) @endphp
                                @foreach($countries as $country)
                                    <span class="badge badge-md bg-secondary p-2 mb-1 text-uppercase">{{ $country }}</span>
                                @endforeach
                            @else
                                <div>No country is selected!</div>
                            @endif
                        </li>
                        <li class="list-group-item">
                            <h5>Selected Sources</h5>
                            @if(!empty($settings) && !empty($settings->sources))
                                @foreach(explode(',', $settings->sources) as $get_source)
                                    <span class="badge badge-md bg-secondary p-2 mb-1 text-uppercase">{{ $get_source }}</span>
                                @endforeach
                            @else
                                <div>No source is selected!</div>
                            @endif
                        </li>
                        <li class="list-group-item">
                            <h5>Expected keywords</h5>
                            <div>{{ !empty($settings) && !empty($settings->keywords) ? $settings->keywords : 'No keyword is set!' }}</div>
                        </li>
                        <li class="list-group-item text-primary">
                            This above criterias will set your newsfeeds in your private newsfeeds page. 
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header bg-white">
                    <h4 class="mt-2">
                        Update Settings
                    </h4>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('settings.store') }}">
                        @csrf
                        <div class="mb-3">
                            <label for="countries" class="form-label">Countries</label>
                            <select class="form-control select-2" id="countries" name="countries[]" multiple="" required>
                                @foreach(countries() as $c => $country)
                                    <option value="{{ $c }}">{{ $country }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="sources" class="form-label">Sources</label>
                            <select class="form-control select-2" id="sources" name="sources[]" multiple="" required>
                                @foreach($sources as $s => $source)
                                    <option value="{{ $source->id }}">{{ $source->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="keywords" class="form-label">Keywords <small class="text-primary">(please use ' , ' for multiple keywords. Ex: science, sports, war updates, ... etc)</small></label>
                            <input class="form-control" type="text" id="keywords" name="keywords" value="">
                        </div>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{ asset('plugins/select2/select2.full.min.js') }}"></script>
    <script type="text/javascript">
        var sources = {!! json_encode(sources()) !!};
        
        $(document).ready(function(){
            $('.select-2').select2({
                placeholder: "Select",
                allowClear: true
            });

            $('.select-2').val('').trigger('change');

            $('#countries').change(function(){
                updateSources($(this).val());
            });

            function updateSources(countries)
            {
                $('#sources').html('');
                var resetSourceInputs = '';
                $.each(sources, function(s, source){

                    if(countries.includes(source['country']))
                    {
                        resetSourceInputs += '<option value="'+source['id']+'">'+source['name']+'</option>'
                    }
                });
                $('#sources').html(resetSourceInputs);
            }
        });
    </script>
@endpush